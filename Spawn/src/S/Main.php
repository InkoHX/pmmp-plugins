<?php
/*
        ///////    ///////  ///////   ///////  ///////      ////////            /////// ///////  //////   ///////
         ///        ///      ///       ///      ///      ///        ///          ///     ///      ///      ///
        ///        //////   ///       ///    ///      ///              ///      ///     ///         /// ///
       ///        ///  /// ///       ////////         ///              ///     ///////////            ///
      ///        ///   //////       ///     ///       ///              ///    ///     ///          ///  ///
     ///        ///      ///       ///        ///        ///        ///      ///     ///        ///       ///
  ///////    ///////  ///////   ///////    ///////          ////////      /////// ///////    ///////   ///////
*/

namespace S;

use pocketmine\{
    event\Listener,
    event\player\PlayerMoveEvent,
    level\Position,
    plugin\PluginBase
};

class Main extends PluginBase implements Listener
{
    public function onEnable()
    {
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
    }

    public function onMove(PlayerMoveEvent $ev)
    {
        $player = $ev->getPlayer();
        if ($ev->getPlayer()->getY() < 0) {
            $x = $this->getServer()->getDefaultLevel()->getSafeSpawn()->getX();
            $y = $this->getServer()->getDefaultLevel()->getSafeSpawn()->getY();
            $z = $this->getServer()->getDefaultLevel()->getSafeSpawn()->getZ();
            $level = $this->getServer()->getDefaultLevel();
            $player->setLevel($level);
            $player->teleport(new Position($x, $y, $z, $level));
        }
    }
}
