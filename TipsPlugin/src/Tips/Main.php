<?php
/*
        ///////    ///////  ///////   ///////  ///////      ////////            /////// ///////  //////   ///////
         ///        ///      ///       ///      ///      ///        ///          ///     ///      ///      ///
        ///        //////   ///       ///    ///      ///              ///      ///     ///         /// ///
       ///        ///  /// ///       ////////         ///              ///     ///////////            ///
      ///        ///   //////       ///     ///       ///              ///    ///     ///          ///  ///
     ///        ///      ///       ///        ///        ///        ///      ///     ///        ///       ///
  ///////    ///////  ///////   ///////    ///////          ////////      /////// ///////    ///////   ///////

    このPluginはInkoHXによって制作されました。
    URL: https://github.com/InkoHX/PocketMine-MP-Plugins
*/
namespace Tips;

use pocketmine\plugin\PluginBase;
use pocketmine\utils\TextFormat;
use Tips\Task\SendTask;

class Main extends PluginBase
{
    public function onEnable()
    {
        $this->getServer()->getScheduler()->scheduleRepeatingTask(new SendTask($this, $this), 180 * 20);
        $this->getLogger()->info(TextFormat::GREEN."読み込み完了");
    }
}
