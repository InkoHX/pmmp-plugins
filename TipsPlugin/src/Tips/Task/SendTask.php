<?php
/**
 * Created by PhpStorm.
 * User: InkoHX
 * Date: 2018/05/28
 * Time: 12:10
 */

namespace Tips\Task;

use pocketmine\plugin\Plugin;
use pocketmine\scheduler\PluginTask;

class SendTask extends PluginTask
{
    public function __construct(Plugin $owner)
    {
        parent::__construct($owner);
    }
    public function onRun(int $currentTick)
    {
        $rand = mt_rand(1, 5);
        if ($rand === 1) {
            $this->owner->getServer()->broadcastMessage('MESSAGE');
        } elseif ($rand === 2) {
            $this->owner->getServer()->broadcastMessage('MESSAGE');
        } elseif ($rand === 3) {
            $this->owner->getServer()->broadcastMessage('MESSAGE');
        } elseif ($rand === 4) {
            $this->owner->getServer()->broadcastMessage('MESSAGE');
        } elseif ($rand === 5) {
            $this->owner->getServer()->broadcastMessage('MESSAGE');
        }
    }
}
